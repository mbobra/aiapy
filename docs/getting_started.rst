Getting Started
================

First, clone the repository

.. code-block:: shell

   git clone https://gitlab.com/LMSAL_HUB/aia_hub/aiapy.git

Next, install the needed dependencies,

.. code-block:: shell

   cd aiapy
   pip install -r requirements/requirements.txt

Finally, install the aiapy package,

.. code-block:: shell

   python setup.py install

Once development is a bit more stable, releases will be made on PyPI and conda-forge